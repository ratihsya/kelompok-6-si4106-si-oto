http://henryaugusta-50298.portmap.host:50298/prk1441/   30/03/2020


SELECT a.*, IFNULL(jml_service_mobil.jmla,0) `Kuota Mobil`, jml_service_motor.jmla `Kuota Motor` , (a.kuota_mobil-IFNULL(jml_service_mobil.jmla,0)) as `Sisa Mobil`,(a.kuota_motor-jml_service_motor.jmla) as `Sisa Motor` FROM service_event a 

LEFT JOIN (SELECT COUNT(*) jmla, service_mobil.id_servis FROM service_mobil GROUP BY service_mobil.id_servis) as jml_service_mobil ON jml_service_mobil.id_servis=a.id 
LEFT JOIN (SELECT COUNT(*) jmla, service_motor.id_servis FROM service_motor GROUP BY service_motor.id_servis) as jml_service_motor ON jml_service_motor.id_servis=a.id
==========================================


CREATE VIEW  `Lihat Sisa Kuota` as SELECT a.*, jml_peserta_ikhwan.jmla `Kuota Ikhwan`, jml_peserta_akhwat.jmla `Kuota Akhwat` , (a.kuota_ikhwan-jml_peserta_ikhwan.jmla) as `Sisa Ikhwan`,(a.kuota_akhwat-jml_peserta_akhwat.jmla) as `Sisa Akhwat` FROM acara_bukber a LEFT JOIN (SELECT COUNT(*) jmla, peserta_ikhwan.id_bukber FROM peserta_ikhwan GROUP BY peserta_ikhwan.id_bukber) as jml_peserta_ikhwan ON jml_peserta_ikhwan.id_bukber=a.id LEFT JOIN (SELECT COUNT(*) jmla, peserta_akhwat.id_bukber FROM peserta_akhwat GROUP BY peserta_akhwat.id_bukber) as jml_peserta_akhwat ON jml_peserta_akhwat.id_bukber=a.id

  public void onResponse(Call<List<newJadwalModel>> call, Response<List<newJadwalModel>> response) {
                kuotaHabis.setVisibility(View.GONE);
                koneksiError.setVisibility(View.GONE);
                loadingJadwal.setVisibility(View.GONE);
                jadwalKosong.setVisibility(View.GONE);
                int total = response.body().size();
                if(total<1){
                    status.setText("Pendaftaran Bukber Belum Dibuka");
                    jadwalKosong.setVisibility(View.VISIBLE);
                    scrollViewUtama.setVisibility(View.GONE);
                }else{
                    koneksiError.setVisibility(View.GONE);
                    containerBawah.setVisibility(View.GONE);
                    for(int i = 0 ; i<response.body().size(); i++){
                        tvID.setText(String.valueOf(response.body().get(i).getId()));
                        tvCatatan.setText(response.body().get(i).getCatatan());
                        tvTanggal.setText(response.body().get(i).getTanggal());
                        tvPorsiIkwan.setText(String.valueOf(response.body().get(i).getKuota_ikhwan()));
                        tvPorsiAkhwat.setText(String.valueOf(response.body().get(i).getKuota_akhwat()));
                        tvStatus.setText(response.body().get(i).getStatus());
                        scrollViewUtama.setVisibility(View.VISIBLE);
                        String kuotaMobil = String.valueOf(response.body().get(i).getKuota_ikhwan());
                        String kuotaMotor = String.valueOf(response.body().get(i).getKuota_akhwat());

                        if(kuotaMobil==null||kuotaMotor==null){
                            kuotaMobil="0";
                            kuotaMotor="0";
                        }

                        int ikuotaMobil=Integer.valueOf(kuotaMobil);
                        int ikuotaMotor=Integer.valueOf(kuotaMotor);

                        if (ikuotaMobil+ikuotaMotor<=0){
                            Toast.makeText(PendaftaranBukber.this, "Kuota Service Sudah Habis", Toast.LENGTH_SHORT).show();
                            kuotaHabis.setVisibility(View.VISIBLE);
                            ketentuanDaftar.setVisibility(View.GONE);
                            updateKuota.setVisibility(View.GONE);
                            containerPeserta.setVisibility(View.GONE);
                        }else{
                            kuotaHabis.setVisibility(View.GONE);
                        }
                    }
                }

            }
            @Override
            public void onFailure(Call<List<newJadwalModel>> call, Throwable t) {
                loadingJadwal.setVisibility(View.GONE);
                koneksiError.setVisibility(View.VISIBLE);
                scrollViewUtama.setVisibility(View.GONE);
            }
        });