<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
        <link rel='icon' href='icon.png' type='image/x-icon'/ >
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet"/>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="CSS/style_index.css">
    <title>Tentang Kami</title>
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
          <a class="navbar-brand" href="index.php">Bukber Ranger</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav ml-auto topnav">
                  <li class="nav-item active">
                      <a class="nav-link" href="donasi.php">Donasi Buka Bersama<span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Lihat Peserta
                      </a>
                      <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                          <a class="dropdown-item" href="show_data_web_ikhwan.php">Ikhwan</a>
                          <a class="dropdown-item" href="show_data_web_akhwat.php">Akhwat</a>
                          <div class="dropdown-divider"></div>
                          <a class="dropdown-item" href="show_data_web.php">Seluruh Peserta</a>
                      </div>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" href="tutorial.php">Tutorial</a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" href="about_us.php">About</a>
                  </li>
                  <li class="nav-item">
                      <a style="margin:3px;"class="btn btn-warning btn-sm text-white" type="button" href="ambil_bukber.php">Ambil Bukber</a>
                  </li>
                  <li class="nav-item">
                      <a style="margin:3px;"class="btn btn-danger btn-sm text-white" type="button" href="index.php">Daftar Bukber</a>
                  </li>

              </ul>
          </div>
      </nav>


<div class="bg-white py-5">
  <div class="container py-5">
    <div class="row align-items-center mb-5">
      <div class="col-lg-6 order-2 order-lg-1">
        <!-- <i class="fa fa-bar-chart fa-2x mb-3 text-primary"></i> -->
        <p class="">Dari Zaid bin Khalid Al-Juhani radhiyallahu ‘anhu, ia berkata bahwa Rasulullah shallallahu ‘alaihi wa sallam bersabda,</p>
        <p>

      مَنْ فَطَّرَ صَائِمًا كَانَ لَهُ مِثْلُ أَجْرِهِ غَيْرَ أَنَّهُ لاَ يَنْقُصُ مِنْ أَجْرِ الصَّائِمِ شَيْئًا

     </p>
    “Siapa memberi makan orang yang berpuasa, maka baginya pahala seperti orang yang berpuasa tersebut, tanpa mengurangi pahala orang yang berpuasa itu sedikit pun juga.” (HR. Tirmidzi no. 807, Ibnu Majah no. 1746, dan Ahmad 5: 192. Al-Hafizh Abu Thahir mengatakan bahwa hadits ini shahih)
    </p>
          <!-- <a href="#" class="btn btn-light px-5 rounded-pill shadow-sm">Learn More</a> -->
      </div>
      <div class="col-lg-5 px-5 mx-auto order-1 order-lg-2"><img src="icon.png" alt="" class="img-fluid mb-4 mb-lg-0"></div>
    </div>
    <div class="row align-items-center">
      <div class="col-lg-6">
        <!-- <i class="fa fa-leaf fa-2x mb-3 text-primary"></i> -->
        <h2 class="font-weight-light">Donasi Buka Bersama Masjid Syamsul Ulum</h2>
        <!-- <p class="font-italic text-muted mb-4">Lebih Mudah dan Cepat ! Akses Bukber Ranger kapanpun dan dimanapun
        melalui perangkat android anda !</p> -->
        <br>
        <button  class="btn btn-primary px-5 shadow-sm btn-block"style="font-size:24px">Donasi Sekarang <i class="fa fa-whatsapp"></i></button>
      </div>
    </div>
  </div>
</div>


<!-- <footer class="bg-light pb-5">
  <div class="container text-center">
    <br>
    <p class="font-italic text-muted mb-0">Provided by Masjid Syamsul Ulum dan PRK Ramadhan Telkom University</p>
  </div>
</footer> -->
  </body>
</html>
