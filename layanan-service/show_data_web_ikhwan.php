<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>

    <link rel='icon' href='icon.png' type='image/x-icon'/ >
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="CSS/style_index.css">
        <title>Peserta Ikhwan</title>
  </head>
  <body>
<!-- start of navbar -->
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
      <a class="navbar-brand" href="index.php">Bukber Ranger</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto topnav">
              <li class="nav-item active">
                  <a class="nav-link" href="show_data_web_ikhwan.php">Peserta Ikhwan<span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Lihat Peserta
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item" href="show_data_web_ikhwan.php">Ikhwan</a>
                      <a class="dropdown-item" href="show_data_web_akhwat.php">Akhwat</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="show_data_web.php">Seluruh Peserta</a>
                  </div>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="tutorial.php">Tutorial</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="donasi.php">Donasi Bukber</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="about_us.php">About</a>
              </li>
              <li class="nav-item">
                  <a style="margin:3px;"class="btn btn-warning btn-sm text-white" type="button" href="ambil_bukber.php">Ambil Bukber</a>
              </li>
              <li class="nav-item">
                  <a style="margin:3px;"class="btn btn-danger btn-sm text-white" type="button" href="index.php">Daftar Bukber</a>
              </li>

          </ul>
      </div>
  </nav>
<!-- end of navbar -->

    <!-- <ul>
      <li><a class="active" href="#home">Home</a></li>
      <li><a href="#news">News</a></li>
      <li><a href="#contact">Peserta</a></li>
      <li><a href="registrasi.php">Daftar Bukber</a></li>
    </ul> -->
    <div class="wrap_info">
      <div class="info_jadwal" >
        <div class="title_info_jadwal">
        </div>
        <?php
          $connect = mysqli_connect('localhost:3307','root','','bukber');
          if($connect){
              $query_cek_jadwal = mysqli_query($connect,"SELECT * from acara_bukber where status = 'tersedia' LIMIT 1");
            if($query_cek_jadwal){
                  $query_ketersediaan=mysqli_query($connect,"SELECT COUNT(*) FROM acara_bukber where status = 'tersedia'");
                  $get_count_ketersediaan=mysqli_fetch_row($query_ketersediaan);
                  $status_tersedia=$get_count_ketersediaan[0];

                  if ($status_tersedia==0 OR $status_tersedia>1) {
                    echo "<h3> Belum Ada Jadwal Bukber Yang Tersedia </h3>";
                  }else{

            $get_jadwal = mysqli_fetch_row($query_cek_jadwal);
            $id_buka_bersama = $get_jadwal[0];
            $cek_jumlah_peserta=mysqli_query($connect,"SELECT COUNT(*) FROM pesertabukber where id_bukber='$id_buka_bersama'");
            $cek_akhwat=mysqli_query($connect,"SELECT COUNT(*) FROM pesertabukber where id_bukber='$id_buka_bersama' AND jenis_kelamin='Ikhwan'");
            $cek_ikhwan=mysqli_query($connect,"SELECT COUNT(*) FROM pesertabukber where id_bukber='$id_buka_bersama' AND jenis_kelamin='Akhwat'");
            $akhwat_no=mysqli_query($connect,"SELECT COUNT(*) FROM pesertabukber where id_bukber='$id_buka_bersama' AND jenis_kelamin='Akhwat' AND status_ambil='Belum Diambil'");
            $ikhwan_no=mysqli_query($connect,"SELECT COUNT(*) FROM pesertabukber where id_bukber='$id_buka_bersama' AND jenis_kelamin='Ikhwan' AND status_ambil='Belum Diambil'");
            $akhwat_yes=mysqli_query($connect,"SELECT COUNT(*) FROM pesertabukber where id_bukber='$id_buka_bersama' AND jenis_kelamin='Akhwat' AND status_ambil='Sudah Diambil'");
            $ikhwan_yes=mysqli_query($connect,"SELECT COUNT(*) FROM pesertabukber where id_bukber='$id_buka_bersama' AND jenis_kelamin='Ikhwan' AND status_ambil='Sudah Diambil'");


              $get_peserta=mysqli_fetch_row($cek_jumlah_peserta);
              $get_ikhwan=mysqli_fetch_row($cek_akhwat);
              $get_akhwat=mysqli_fetch_row($cek_ikhwan);
              $get_remain_ikhwan=mysqli_fetch_row($ikhwan_no);
              $get_remain_akhwat=mysqli_fetch_row($akhwat_no);
              $get_taken_ikhwan=mysqli_fetch_row($ikhwan_yes);
              $get_taken_akhwat=mysqli_fetch_row($akhwat_yes);
              // echo "ID Bukber : $get_jadwal[0] <br>";

              echo "<strong>$get_jadwal[1]</strong><br>";
              echo "Tanggal : $get_jadwal[2] <br>";
              // echo "Menu Makanan : $get_jadwal[5] <br>";
              echo "Total Peserta : $get_peserta[0] Orang <br>";
              echo "Peserta Ikhwan : $get_ikhwan[0] Orang <br>" ;
              echo "Peserta Akhwat : $get_akhwat[0] Orang <br>";
              echo "<strong>Peserta Belum Mengambil :</strong><br>";
              echo "Ikhwan : $get_remain_ikhwan[0] Orang <br>" ;
              // echo "Akhwat: $get_remain_akhwat[0] Orang <br>" ;
              echo "<strong>Peserta Sudah Mengambil :</strong><br>";
              echo "Ikhwan : $get_taken_ikhwan[0] Orang <br>" ;
              // echo "Akhwat : $get_taken_akhwat[0] Orang <br>" ;
          }
            }else{
            echo "<h1>Belum Tersedia Jadwal Buka Bersama";
            }

            }else{
              echo "<p>Status Koneksi : Gagal Terhubung ke Database</p>";
              echo "<p>Silakan Coba Lagi Nanti</p>";
            }

         ?>
   </div>
       </div>

    <div class="containerBtn">
      <div style="text-align:center"class="">
        <div class="">

          <table width="100%" style="margin:10px;padding:5px'">
            <tr>
              <td width="90%"><input type="text" id="query" class="form-control" placeholder="Cari Peserta" required></td>
              <td style="padding:10px;"width="10%"><i class="fa fa-search"></i></td>
            </tr>
          </table>
        </div>

      <button class="btnTheme" type="button" id="btn_search" name="button">CARI</button>
      <button class="btnTheme" id="btn_reset" type="button" name="button">RESET</button>
      <button onClick="history.go(0);" class="btnTheme" id="btn_refresh" type="button" name="button">REFRESH</button>
    </div>
  </div>

    </div>
        <div style="margin:15px; align:center">
          <form>
            <div>
          </div>
        </div>
    <div style="overflow-x:auto;margin:15px;" >
    <table id="tb_peserta" width="100%">
      <thead>
        <th style="font-family:montserrat">ID.</th>
        <th>Nama</td>
        <th>Username</th>
        <th>Kategori</th>
        <th>Status Ambil</th>
      </thead>
      <tbody  id="tb_pesertax">
        <?php
        $connect = mysqli_connect('localhost:3307','root','','bukber');
        if($connect){
          $call_database=mysqli_query($connect,"SELECT a.id_peserta,a.nama,a.username,a.password,a.jenis_kelamin , a.status_ambil as Status
            FROM pesertabukber a join acara_bukber b on a.id_bukber=b.id where b.status = 'tersedia' and a.jenis_kelamin='Ikhwan'");
            $query_ketersediaan=mysqli_query($connect,"SELECT COUNT(*) FROM acara_bukber where status = 'tersedia'");
            $get_count_ketersediaan=mysqli_fetch_row($query_ketersediaan);
            $status_tersedia=$get_count_ketersediaan[0];
            if ($status_tersedia==0 OR $status_tersedia>1) {
            //Jika Tidak Ada Jadwal === SHOW NOTHING
            }else{
              $get_peserta=mysqli_fetch_row($cek_jumlah_peserta);
              $get_ikhwan=mysqli_fetch_row($cek_akhwat);
              $get_akhwat=mysqli_fetch_row($cek_ikhwan);

                  if($call_database){
                  while ($fill_row = mysqli_fetch_array($call_database)) {
                    echo "<tr><td>$fill_row[0]</td>
                    <td>$fill_row[1]</td>
                    <td>$fill_row[2]
                    </td><td>$fill_row[4]</td>
                    <td>$fill_row[5]</td>
                    </tr>";
                  }
                    }else{
                      echo "Gagal Terhubung Dengan Server";
                    }
          }
          }else{
          echo "<p>Status Koneksi : Gagal Terhubung ke Database</p>";
          echo "<p>Silakan Coba Lagi Nanti</p>";
        }
          ?>
        </tbody>
      </table>

    </div>



    <script>
         $(document).ready(function(){
              $('#btn_search').click(function(){
                   search_table($('#query').val());
              });
              $('#btn_reset').click(function(){
                   search_table('');
                   $('#query').val('');
              });
              function search_table(value){
                   $('#tb_pesertax tr').each(function(){
                        var found = 'false';
                        $(this).each(function(){
                             if($(this).text().toLowerCase().indexOf(value.toLowerCase()) >= 0)
                             {
                                  found = 'true';
                             }
                        });
                        if(found == 'true')
                        {
                             $(this).show();
                        }
                        else
                        {
                             $(this).hide();
                        }
                   });
              }
         });
    </script>

  </body>
  <footer>
  </footer>
</html>
